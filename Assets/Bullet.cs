﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _bulletVelocity = 0.1f;
    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void SetVelocity() 
    {
        rb.velocity = new Vector3(0, _bulletVelocity, 0);
    }
}
