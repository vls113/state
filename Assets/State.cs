﻿using UnityEngine;

public abstract class State 
{
    public abstract void Enter();
    public virtual void UpdateState() { }
    public abstract void Exit();
}
