﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) 
        {
            if (Pool.Instance.Get("Bullet") != null)
            {
                var bullet = Pool.Instance.Get("Bullet");
                bullet.transform.position = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                bullet.SetActive(true);
                bullet.GetComponent<Bullet>().SetVelocity();
            }
        }       
    }
}