﻿using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PoolObject
{
    public GameObject prefab;
    public int amount;
}

public class Pool : MonoBehaviour
{
    public static Pool Instance = null;

    public List<PoolObject> poolObjects;
    public List<GameObject> objectsOnScene;

    [SerializeField, Header("Transform спавнера")] 
    private Transform spawner;

    void Start()
    {

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        foreach (PoolObject item in poolObjects) 
        {
            for (int i = 0; i < item.amount; i++)
            {
                GameObject obj = Instantiate(item.prefab, spawner);
                objectsOnScene.Add(obj);
                obj.SetActive(false);
            }
        }
    }

    public GameObject Get(string tag) 
    {
        foreach (GameObject item in objectsOnScene) 
        {
            if (item.CompareTag(tag) && !item.activeInHierarchy) 
            {
                return item;
            }
        }
        return null;
    }
}
